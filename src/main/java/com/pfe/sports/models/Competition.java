package com.pfe.sports.models;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Competition {

  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  private String name;

  private String description;

  private String sportType;

  private LocalDateTime startEvent;

  private LocalDateTime endEvent;

  private String latitude;

  private String longitude;

  private String logo;

  @ManyToOne
  private User createdBy;


}
