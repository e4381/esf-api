package com.pfe.sports.models;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {

  @Id
  @Column(unique = true, nullable = false)
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  private LocalDateTime createdDate = LocalDateTime.now();

  private Boolean isActive = true;

  private String firstName;

  private String lastName;

  private String email;

  private String password;

  private String adresse;

  private String login;

  private String logo;

  @Enumerated(EnumType.STRING)
  @Column(length = 20)
  private ERole role;

  private String sportType;

  @ManyToOne
  private TrainingGroup trainingGroup;


}
