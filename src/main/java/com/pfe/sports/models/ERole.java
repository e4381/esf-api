package com.pfe.sports.models;

public enum ERole {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN,
  ROLE_GUEST,
  ROLE_SPORT
}
