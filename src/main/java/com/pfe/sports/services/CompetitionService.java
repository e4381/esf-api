package com.pfe.sports.services;

import com.pfe.sports.models.Competition;
import com.pfe.sports.repository.CompetitionRepository;
import com.pfe.sports.repository.CompetitionResultRepository;
import com.pfe.sports.repository.RatingRepository;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.management.openmbean.KeyAlreadyExistsException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CompetitionService {

  private final CompetitionRepository competitionRepository;
  private final CompetitionResultRepository competitionResultRepository;
  private final RatingRepository ratingRepository;

  public CompetitionService(CompetitionRepository competitionRepository,
                            CompetitionResultRepository competitionResultRepository,
                            RatingRepository ratingRepository) {
    this.competitionRepository = competitionRepository;
    this.competitionResultRepository = competitionResultRepository;
    this.ratingRepository = ratingRepository;
  }

  public List<Competition> findAllCompetition() {
    return competitionRepository.findAll();
  }

  public Competition createCompetition(Competition competition) {
    Competition existCompetition =
        competitionRepository.findCompetitionByName(competition.getName());
    if(existCompetition!= null && Objects.equals(existCompetition.getName(), competition.getName())) {
      if(!Objects.equals(existCompetition.getId(), competition.getId())){
        throw new KeyAlreadyExistsException("le nom est deja utiliser");
      }
    }
    return competitionRepository.save(competition);
  }

  public Optional<Competition> findCompetitionById(String competitionId) {
    return competitionRepository.findById(competitionId);
  }

  public void deleteCompetition(String competitionId) {
    competitionResultRepository.deleteAllByCompetitionId(competitionId);
    ratingRepository.deleteAllByCompetitionId(competitionId);
    competitionRepository.deleteById(competitionId);
  }

}
