package com.pfe.sports.services;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Random;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FilesStorageService {

  private final Path root = Paths.get("images");

  public String uploadFile(MultipartFile file) {
    try {
      int upperbound = 25000000;
      Random rand = new Random();
      Integer int_random = rand.nextInt(upperbound);
      String fileName = Objects.requireNonNull(file.getOriginalFilename())
          .replaceAll("\\s+", "");
      fileName = int_random + fileName;
      Files.copy(file.getInputStream(), this.root.resolve(fileName));
      return "http://127.0.0.1:8080/api/users/files/" + fileName;
    } catch (Exception e) {
      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
    }
  }

  public Resource loadFile(String filename) {
    try {
      Path file = root.resolve(filename);
      Resource resource = new UrlResource(file.toUri());

      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Could not read the file!");
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Error: " + e.getMessage());
    }
  }
}
