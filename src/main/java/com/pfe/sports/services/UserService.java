package com.pfe.sports.services;

import com.pfe.sports.models.ERole;
import com.pfe.sports.models.User;
import com.pfe.sports.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public List<User> findAllUser() {
    return userRepository.findAll();
  }

  public List<User> findUsersByTrainingGroup(String groupId) {
    return userRepository.findAllByTrainingGroupId(groupId);
  }
  public User findUsersByTrainingGroup(String groupId, String id) {
    return userRepository.findAllByTrainingGroupIdAndId(groupId, id);
  }

  public User findUserById(String id){
    return  userRepository.findById(id).orElse(null);
  }

  public boolean disableUser(String userId) {
    Optional<User> user = userRepository.findById(userId);
    if (user.isPresent()) {
      User getUser = user.get();
      getUser.setIsActive(false);
      userRepository.save(getUser);
      return true;
    }
    return false;
  }

  public boolean enableUser(String userId) {
    Optional<User> user = userRepository.findById(userId);
    if (user.isPresent()) {
      User getUser = user.get();
      getUser.setIsActive(true);
      userRepository.save(getUser);
      return true;
    }
    return false;
  }


  public boolean deleteUser(String userId) {
    if (userRepository.existsById(userId)) {
      userRepository.deleteById(userId);
      return true;
    }
    return false;
  }

  public User updateUser(User user) {
    return userRepository.save(user);
  }

  public List<User> findAllByRole(String type) {
    return userRepository.findAllByRole(ERole.valueOf(type));
  }
}
