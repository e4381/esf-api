package com.pfe.sports.services;

import com.pfe.sports.models.CompetitionResult;
import com.pfe.sports.models.User;
import java.time.format.DateTimeFormatter;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailService{

    @Autowired
    public JavaMailSender emailSender;

    public Boolean sendcreatedMail(User user) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        StringBuilder htmlBody = new StringBuilder("");
        htmlBody.append("<head>"+
            "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
            "\n" +
            "<!-- Optional theme -->\n" +
            "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">\n" +
            "\n" +
            "<!-- Latest compiled and minified JavaScript -->\n" +
            "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>"+
            "<meta http-equiv=\"Content-Type\"  content=\"text/html charset=UTF-8\" />"+
            "</head>"+
            "<body>"+
            "<div class='container'>"+
            "<div class='row'> <h1>Cher "+user.getLogin()+"</h1></div>"+
            "<div class='row'>  </div>"+
            "<div class='row'> Félicitation, Pour terminer le processus de création de votre compte <br> Veuillez utilisez le bouton ci-dessous pour s'authentifier avec ce mot de passe <strong>"+user.getPassword()+"</strong>.</div>"+
            "<br> <div class='row'>" +
            "<div class='col-md-2'></div>"+
            "<div class='col-md-4'>"+
            "  <a href='http://localhost:4200/auth' class='btn btn-success'>Authentifiez-vous</a>\n" +
            "</div>"+
            "</div>"+
            "<br> "+
            "<br>"+
            "<div class='row'>L'&eacute;quipe ESF</div>"+
            "<hr>"+
            "<br>"+
            "<div class='row'> Si vous rencontrez des probl&egrave;mes avec le bouton ci-dessus, copiez et collez l’URL ci-dessous dans votre navigateur Web. </div>"+
            "<br>"+
            "<div class='row'> <div class='col-10'> <a style='display:flex;word-break: break-all;' href='http://localhost:4200/auth'><span>http://localhost:4200/auth</span></a> </div> </div>"+
            " </body>"+
            "</html>");
        message.setContent(htmlBody.toString(), "text/html");

        helper.setTo(user.getEmail());

        helper.setSubject("Votre compte");


        this.emailSender.send(message);
        return true;
    }

    public Boolean sendcreatedMail(CompetitionResult competitionResult) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
        StringBuilder htmlBody = new StringBuilder("");
        htmlBody.append("<html>\n" +
            "  <head>\n" +
            "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
            "    <style>\n" +
            "    \tbody{margin-top:20px;}\n" +
            "       /* Add custom classes and styles that you want inlined here */\n" +
            "    </style>\n" +
            "  </head>\n" +
            "  <body class=\"bg-light\">\n" +
            "<table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\" bgcolor=\"#f6f6f6\">\n" +
            "    <tbody>\n" +
            "        <tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">\n" +
            "            <td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>\n" +
            "            <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\"\n" +
            "                valign=\"top\">\n" +
            "                <div class=\"content\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\">\n" +
            "                    <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;\"\n" +
            "                        bgcolor=\"#fff\">\n" +
            "                        <tbody>\n" +
            "                            <tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">\n" +
            "                                <td class=\"\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #009b7c; margin: 0; padding: 20px;\"\n" +
            "                                    align=\"center\" bgcolor=\"#71b6f9\" valign=\"top\">\n" +
            "                                       <span style=\"justify-content: left;display: flex;\"> vous ete invite a une competition </span>\n" +
            "                                       <span style=\"justify-content: left;display: flex;\">  Nom : "+ competitionResult.getCompetition().getName() +"</span>\n" +
            "                                       <span style=\"justify-content: left;display: flex;\"> le  : "+ competitionResult.getCompetition().getStartEvent()+"</span>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "</table>\n" +
            "  </body>\n" +
            "</html>\n");
        message.setContent(htmlBody.toString(), "text/html");

        helper.setTo(competitionResult.getUser().getEmail());

            helper.setSubject("Votre invitation");


        this.emailSender.send(message);
        return true;
    }

}
