package com.pfe.sports.services;

import com.pfe.sports.models.Rating;
import com.pfe.sports.repository.RatingRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

  public final RatingRepository  ratingRepository;

  public RatingService(RatingRepository ratingRepository) {
    this.ratingRepository = ratingRepository;
  }

  public Rating findRatingByUserIdAndCompetitionId(String userId, String competitionId) {
    return ratingRepository.findRatingByUserIdAndCompetitionId(userId, competitionId);
  }

  public Rating createRating(Rating rating) {
    return ratingRepository.save(rating);
  }

  public List<?> findTopRating() {
    return ratingRepository.findTopRating();
  }
}
