package com.pfe.sports.services;

import com.pfe.sports.models.Comment;
import com.pfe.sports.repository.CommentRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class CommentService {

  private final CommentRepository commentRepository;

  public CommentService(CommentRepository commentRepository) {
    this.commentRepository = commentRepository;
  }

  public List<Comment> findAllByCompetitionIdAndUserId(String competitionId, String userId){
    return commentRepository.findAllByCompetitionIdAndUserId(competitionId, userId);
  }

  public Comment createComment(Comment comment) {
    return commentRepository.save(comment);
  }

  public List<?> CountComment() {
    return commentRepository.CountComment();
  }



}
