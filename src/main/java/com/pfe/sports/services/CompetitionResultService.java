package com.pfe.sports.services;

import com.pfe.sports.models.CompetitionResult;
import com.pfe.sports.models.User;
import com.pfe.sports.repository.CompetitionResultRepository;
import com.pfe.sports.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompetitionResultService {

  private final CompetitionResultRepository competitionResultRepository;
  private final UserRepository userRepository;

  @Autowired
  MailService mailService;

  public CompetitionResultService(
      CompetitionResultRepository competitionResultRepository,
      UserRepository userRepository) {
    this.competitionResultRepository = competitionResultRepository;
    this.userRepository = userRepository;
  }

  public List<CompetitionResult> findAllByUserId(String userId) {
    return competitionResultRepository.findAllByUserId(userId);
  }

  public CompetitionResult createCompetitionResult(CompetitionResult competitionResult)
      throws MessagingException {
    Optional<User> user = userRepository.findById(competitionResult.getUser().getId());
    competitionResult.setUser(user.get());
    mailService.sendcreatedMail(competitionResult);
    return competitionResultRepository.save(competitionResult);
  }

  public CompetitionResult findCompetitionResultByCompetitionIdAndUserId(String competitionId,
                                                                         String userId) {
    return competitionResultRepository.findCompetitionResultByCompetitionIdAndUserId(competitionId,
        userId);
  }

  public List<CompetitionResult> findAllCompetitionResultByCompetitionId(String competitionId) {
    return competitionResultRepository.findAllByCompetitionId(competitionId);
  }

  public void deletePartisipation(String id) {
    competitionResultRepository.deleteById(id);
  }

  public List<CompetitionResult> findAllCompetitionResult() {
    return competitionResultRepository.findAll();
  }
}
