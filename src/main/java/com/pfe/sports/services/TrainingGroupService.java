package com.pfe.sports.services;

import com.pfe.sports.models.TrainingGroup;
import com.pfe.sports.models.User;
import com.pfe.sports.repository.TrainingGroupRepository;
import com.pfe.sports.repository.UserRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TrainingGroupService {

  private final TrainingGroupRepository trainingGroupRepository;
  private final UserRepository userRepository;

  public TrainingGroupService(
      TrainingGroupRepository trainingGroupRepository,
      UserRepository userRepository) {
    this.trainingGroupRepository = trainingGroupRepository;
    this.userRepository = userRepository;
  }

  public TrainingGroup findTrainingGroupById(String id) {
    return trainingGroupRepository.findTrainingGroupById(id);
  }

  public List<TrainingGroup> findAllTrainingGroup() {
    return trainingGroupRepository.findAll();
  }

  public TrainingGroup createTrainingGroup(TrainingGroup trainingGroup) {
    return trainingGroupRepository.save(trainingGroup);
  }

  public void deleteTrainingGroupById(String id) {
    List<User> users = userRepository.findAllByTrainingGroupId(id);
    users.forEach(user -> user.setTrainingGroup(null));
    userRepository.saveAll(users);
    trainingGroupRepository.deleteById(id);
  }
}
