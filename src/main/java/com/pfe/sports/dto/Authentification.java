package com.pfe.sports.dto;

import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Authentification {
  @NotBlank
  private String username;

  @NotBlank
  private String password;

}
