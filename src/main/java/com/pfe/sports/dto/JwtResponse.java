package com.pfe.sports.dto;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JwtResponse {
  private String token;
  private String username;
  private String role;
  private boolean enable;

  public JwtResponse(String token, String username, String role, boolean enable) {
    this.token = token;
    this.username = username;
    this.role = role;
    this.enable = enable;
  }
}
