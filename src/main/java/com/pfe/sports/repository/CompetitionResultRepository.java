package com.pfe.sports.repository;

import com.pfe.sports.models.CompetitionResult;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CompetitionResultRepository extends JpaRepository<CompetitionResult, String> {

  List<CompetitionResult> findAllByUserId(String userId);
  List<CompetitionResult> findAllByCompetitionId(String id);
  void deleteAllByCompetitionId(String id);
  CompetitionResult findCompetitionResultByCompetitionIdAndUserId(String competitionId,
                                                                  String userId);
}
