package com.pfe.sports.repository;

import com.pfe.sports.models.Rating;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface RatingRepository extends JpaRepository<Rating, String> {
  Rating findRatingByUserIdAndCompetitionId(String userId, String competitionId);

  void deleteAllByCompetitionId(String id);

  @Query(value =
      "select competition_id, co.name , sum(number) from rating r, competition co " +
          "where co.id = r.competition_id " +
          "group by competition_id , co.name " +
          "order by sum(number) desc " +
          "limit 7", nativeQuery = true)
  List<?> findTopRating();
}
