package com.pfe.sports.repository;

import com.pfe.sports.models.TrainingGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingGroupRepository extends JpaRepository<TrainingGroup, String> {
  TrainingGroup findTrainingGroupById(String id);
}
