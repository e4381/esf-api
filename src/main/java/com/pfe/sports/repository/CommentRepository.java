package com.pfe.sports.repository;

import com.pfe.sports.models.Comment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CommentRepository extends JpaRepository<Comment, String> {

  List<Comment> findAllByCompetitionIdAndUserId(String competitionId, String userId);

  @Query(value = "select competition_id, co.name , count(*) from comment cm, competition co " +
      "where co.id = cm.competition_id " +
      "group by competition_id , co.name " +
      "order by count(*) desc ", nativeQuery = true)
  List<?> CountComment();
}
