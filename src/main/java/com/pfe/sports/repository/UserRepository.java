package com.pfe.sports.repository;

import com.pfe.sports.models.ERole;
import com.pfe.sports.models.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
  Optional<User> findUserByLogin(String login);
  boolean existsByLogin(String login);
  boolean existsByEmail(String email);
  List<User> findAllByTrainingGroupId(String groupId);
  User findAllByTrainingGroupIdAndId(String groupId, String userId);
  List<User> findAllByRole(ERole role);
}
