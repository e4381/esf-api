package com.pfe.sports.repository;

import com.pfe.sports.models.Competition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompetitionRepository extends JpaRepository<Competition, String> {
  Competition findCompetitionByName(String Name);
}
