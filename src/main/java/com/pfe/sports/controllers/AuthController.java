package com.pfe.sports.controllers;

import com.pfe.sports.dto.Authentification;
import com.pfe.sports.dto.JwtResponse;
import com.pfe.sports.models.User;
import com.pfe.sports.repository.UserRepository;
import com.pfe.sports.security.jwt.JwtUtils;
import com.pfe.sports.security.services.UserDetailsImpl;
import com.pfe.sports.services.MailService;
import javax.mail.MessagingException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @Autowired
  MailService mailService;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody Authentification authentification) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(authentification.getUsername(),
            authentification.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    String roles = userDetails.getAuthorities().stream()
        .map(GrantedAuthority::getAuthority).findAny().get();
    return ResponseEntity.ok(new JwtResponse(jwt,
        userDetails.getUsername(),
        roles, userDetails.isActive()));
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody User signUpRequest)
      throws MessagingException {
    if(signUpRequest.getId() == null){
      if (userRepository.existsByLogin(signUpRequest.getLogin())) {
        return ResponseEntity
            .badRequest()
            .body("Ce login est deja utilisé, veuillez saisir un autre");
      }

      if (userRepository.existsByEmail(signUpRequest.getEmail())) {
        return ResponseEntity
            .badRequest()
            .body("Cette adresse email est deja utilisé, veuillez saisir un autre");
      }
    }
    mailService.sendcreatedMail(signUpRequest);
    // Create new user's account
    signUpRequest.setPassword(encoder.encode(signUpRequest.getPassword()));

    return ResponseEntity.ok(userRepository.save(signUpRequest));
  }
}
