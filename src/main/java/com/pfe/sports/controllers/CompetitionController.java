package com.pfe.sports.controllers;

import com.pfe.sports.models.Competition;
import com.pfe.sports.models.User;
import com.pfe.sports.services.CompetitionService;
import com.pfe.sports.services.FilesStorageService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/competitions")
public class CompetitionController {

  private final CompetitionService competitionService;
  private final FilesStorageService storageService;

  public CompetitionController(CompetitionService competitionService,
                               FilesStorageService storageService) {
    this.competitionService = competitionService;
    this.storageService = storageService;
  }

  @GetMapping("")
  public List<Competition> findAllCompetition() {
    return competitionService.findAllCompetition();
  }

  @PostMapping("")
  public Competition createCompetition(@RequestBody @Valid Competition competition) {
    return competitionService.createCompetition(competition);
  }

  @GetMapping("/{competitionId}")
  public Competition findCompetitionById(@PathVariable @Valid String competitionId) {
    Optional<Competition> optionalCompetition = competitionService.findCompetitionById(competitionId);
    return optionalCompetition.orElse(null);
  }

  @DeleteMapping("/{competitionId}")
  public ResponseEntity<?> deleteCompetition(@PathVariable @Valid String competitionId) {
    try {
      competitionService.deleteCompetition(competitionId);
      return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }catch (Exception exception) {
      return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/upload/{id}")
  public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") String userId) {
    String message = "";
    try {
      Optional<Competition> optionalCompetition = competitionService.findCompetitionById(userId);
      if(optionalCompetition.isPresent()){
        Competition competition = optionalCompetition.get();
        competition.setLogo(storageService.uploadFile(file));
        competitionService.createCompetition(competition);
        message = "Uploaded the file successfully: " + file.getOriginalFilename();
        return message;
      }else{
        throw new ResourceNotFoundException("user not exist");
      }
    } catch (Exception e) {
      message = "Could not upload the file: " + file.getOriginalFilename() + "! with error: "+e.getMessage();
      throw new ResourceNotFoundException(message);
    }
  }

}
