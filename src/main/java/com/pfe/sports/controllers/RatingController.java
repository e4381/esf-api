package com.pfe.sports.controllers;

import com.pfe.sports.models.Comment;
import com.pfe.sports.models.Rating;
import com.pfe.sports.repository.RatingRepository;
import com.pfe.sports.services.RatingService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/ratings")
public class RatingController {

  private final RatingService ratingService;

  public RatingController(RatingService ratingService) {
    this.ratingService = ratingService;
  }

  @GetMapping("/competition/{competitionid}/user/{userid}")
  public Rating findRatingByCompetitionId(@PathVariable @Valid String competitionid, @PathVariable @Valid String userid) {
    return ratingService.findRatingByUserIdAndCompetitionId(userid, competitionid);
  }

  @PostMapping("")
  public Rating createRating(@RequestBody @Valid Rating rating) {
    return ratingService.createRating(rating);
  }

  @GetMapping("/top")
  public List<?> findTopRating() {
    return ratingService.findTopRating();
  }

}
