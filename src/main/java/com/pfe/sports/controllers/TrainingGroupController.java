package com.pfe.sports.controllers;

import com.pfe.sports.models.TrainingGroup;
import com.pfe.sports.services.TrainingGroupService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/groups")
public class TrainingGroupController {

  private final TrainingGroupService trainingGroupService;

  public TrainingGroupController(TrainingGroupService trainingGroupService) {
    this.trainingGroupService = trainingGroupService;
  }

  @GetMapping("")
  public List<TrainingGroup> findAllTrainingGroup() {
    return trainingGroupService.findAllTrainingGroup();
  }

  @PostMapping("")
  public TrainingGroup createTrainingGroup(@RequestBody @Valid TrainingGroup trainingGroup) {
    return trainingGroupService.createTrainingGroup(trainingGroup);
  }

  @GetMapping("/{id}")
  public TrainingGroup findTrainingGroupById(@PathVariable @Valid String id) {
    return trainingGroupService.findTrainingGroupById(id);
  }

  @DeleteMapping("/{id}")
  public void deleteTrainingGroupById(@PathVariable @Valid String id) {
    trainingGroupService.deleteTrainingGroupById(id);
  }
}
