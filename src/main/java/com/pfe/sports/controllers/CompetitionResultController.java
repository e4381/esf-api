package com.pfe.sports.controllers;

import com.pfe.sports.models.Comment;
import com.pfe.sports.models.CompetitionResult;
import com.pfe.sports.services.CompetitionResultService;
import java.util.List;
import javax.mail.MessagingException;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/competition_result")
public class CompetitionResultController {
  private final CompetitionResultService competitionResultService;

  public CompetitionResultController(CompetitionResultService competitionResultService) {
    this.competitionResultService = competitionResultService;
  }

  @GetMapping("/user/{userid}")
  public List<CompetitionResult> findAllByUserId(@PathVariable @Valid String userid) {
    return competitionResultService.findAllByUserId(userid);
  }

  @GetMapping("/competition/{competitionid}/user/{userid}")
  public CompetitionResult findCompetitionResultByCompetitionIdAndUserId(
      @PathVariable @Valid String competitionid, @PathVariable @Valid String userid) {
    return competitionResultService.findCompetitionResultByCompetitionIdAndUserId(competitionid,
        userid);
  }

  @GetMapping("/competition/{competitionid}")
  public List<CompetitionResult> findAllCompetitionResultByCompetitionId(
      @PathVariable @Valid String competitionid) {
    return competitionResultService.findAllCompetitionResultByCompetitionId(competitionid);
  }

  @PostMapping("")
  public CompetitionResult createCompetitionResult(
      @RequestBody @Valid CompetitionResult competitionResult) throws MessagingException {
    return competitionResultService.createCompetitionResult(competitionResult);
  }

  @DeleteMapping("/{competitionresultid}")
  public void deletePartisipation(@PathVariable @Valid String competitionresultid) {
    competitionResultService.deletePartisipation(competitionresultid);
  }

  @GetMapping("")
  public List<CompetitionResult> findAllCompetitionResult() {
    return competitionResultService.findAllCompetitionResult();
  }
}
