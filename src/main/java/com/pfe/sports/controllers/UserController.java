package com.pfe.sports.controllers;

import com.pfe.sports.models.User;
import com.pfe.sports.services.FilesStorageService;
import com.pfe.sports.services.UserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users")
public class UserController {

  private final UserService userService;
  private final FilesStorageService storageService;

  public UserController(UserService userService,
                        FilesStorageService storageService) {
    this.userService = userService;
    this.storageService = storageService;
  }

  @GetMapping("/role/{role}")
  public List<User> findAllUserByRole(@PathVariable @Valid String role) {
    return userService.findAllByRole(role);
  }

  @GetMapping("/{id}")
  public User findUserById(@PathVariable @Valid String id) {
    return userService.findUserById(id);
  }

  @GetMapping("")
  public List<User> findAllUser() {
    return userService.findAllUser();
  }

  @DeleteMapping("/{id}")
  public void deleteUserById(@PathVariable @Valid String id) {
    userService.deleteUser(id);
  }

  @PatchMapping("/disableUser/{id}")
  public void disableUser(@PathVariable @Valid String id) {
    userService.disableUser(id);
  }

  @PatchMapping("/enableUser/{id}")
  public void enableUser(@PathVariable @Valid String id) {
    userService.enableUser(id);
  }

  @PutMapping("")
  public User updateUser(@RequestBody @Valid User user) {
    return userService.updateUser(user);
  }

  @GetMapping("{userid}/trainingGroup/{id}")
  public User findUsersByTrainingGroupAndUserId(@PathVariable @Valid String id, @PathVariable @Valid String userid) {
    return userService.findUsersByTrainingGroup(id, userid);
  }

  @GetMapping("/trainingGroup/{id}")
  public List<User> findUsersByTrainingGroup(@PathVariable @Valid String id) {
    return userService.findUsersByTrainingGroup(id);
  }

  @PostMapping("/upload/{id}")
  public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") String userId) {
    String message = "";
    try {
      User user = userService.findUserById(userId);
      if(user !=null ){
        user.setLogo(storageService.uploadFile(file));
        userService.updateUser(user);
        message = "Uploaded the file successfully: " + file.getOriginalFilename();
        return message;
      }else{
        throw new ResourceNotFoundException("user not exist");
      }
    } catch (Exception e) {
      message = "Could not upload the file: " + file.getOriginalFilename() + "! with error: "+e.getMessage();
      throw new ResourceNotFoundException(message);
    }
  }

  @GetMapping("/files/{fileName}")
  @ResponseBody
  public ResponseEntity<Resource> getFile(@PathVariable("fileName") String fileName) {
    Resource file = storageService.loadFile(fileName);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
  }


}
