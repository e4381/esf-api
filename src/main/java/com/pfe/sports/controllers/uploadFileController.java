package com.pfe.sports.controllers;

import com.pfe.sports.services.FilesStorageService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/files")
public class uploadFileController {

  private final FilesStorageService filesStorageService;

  public uploadFileController(FilesStorageService filesStorageService) {
    this.filesStorageService = filesStorageService;
  }
}
