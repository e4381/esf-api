package com.pfe.sports.controllers;

import com.pfe.sports.models.Comment;
import com.pfe.sports.models.Competition;
import com.pfe.sports.services.CommentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/commets")
public class CommetController {

  private final CommentService commentService;

  public CommetController(CommentService commentService) {
    this.commentService = commentService;
  }

  @GetMapping("/competition/{competitionid}/user/{userid}")
  public List<Comment> findAllCommetByCompetitionId(@PathVariable @Valid String competitionid,
                                                    @PathVariable @Valid String userid) {
    return commentService.findAllByCompetitionIdAndUserId(competitionid, userid);
  }

  @PostMapping("")
  public Comment createComment(@RequestBody @Valid Comment comment) {
    return commentService.createComment(comment);
  }

  @GetMapping("/top")
  public List<?> CountComment() {
    return commentService.CountComment();
  }



}
